import 'package:flutter/material.dart';

class AspectRatioExample extends StatelessWidget {
  const AspectRatioExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AspectRatio(
        aspectRatio: 1.5,
        child: Container(
          height: 200,
          width: 300,
          color: Colors.green[200],
          child: const FlutterLogo(),
        ),
      ),
    );
  }
}

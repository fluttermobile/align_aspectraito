import 'package:flutter/material.dart';

void main() {
  runApp(const AlignExample());
}

class AlignExample extends StatelessWidget {
  const AlignExample({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Container(
            color: Colors.yellow,
            height: 200.0,
            width: 600,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                color: Colors.green,
                child: const Text("Align me!"),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
